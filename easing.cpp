#include "easing.h"

// values of t need to range between 0.0 - 1.0

float Easing::linear(float t) {
	return t;
}

float Easing::easeInQuad(float t) {
	return t*t;
}

float Easing::easeOutQuad(float t) {
	return t*(2-t);
}

float Easing::easeInOutQuad(float t) {
	return t < 0.5f ? 2*t*t : -1+(4-2*t)*t; 
}

float Easing::easeInCubic(float t) {
	return t*t*t;
}

float Easing::easeOutCubic(float t) {
	return (--t)*t*t+1;
}

float Easing::easeInOutCubic(float t) {
	return t < 0.5f ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1;
}

float Easing::easeInQuart(float t) {
	return t*t*t*t;
}

float Easing::easeOutQuart(float t) {
	return 1-(--t)*t*t*t;
}

float Easing::easeInOutQuart(float t) {
	return t < 0.5f ? 8*t*t*t*t : 1-8*(--t)*t*t*t;
}

float Easing::easeInQuint(float t) {
	return t*t*t*t*t;
}

float Easing::easeOutQuint(float t) {
	return 1+(--t)*t*t*t*t;
}

float Easing::easeInOutQuint(float t) {
	return t < 0.5f ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t;
}
