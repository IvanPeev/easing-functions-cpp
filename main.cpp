#include <iostream>
#include <math.h>
#include <vector>
#include <SFML/Graphics.hpp>
#include "easing.h"

#define WIDTH 1280
#define HEIGHT 720
#define FPS_60 1000.f / 60.f

#define LIMIT 100
#define LIMIT_F 100.f

sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "Easing functions");
bool direction = false;
int i = 0;

float radius = 0,
			baseRadius = 350;

std::vector<sf::CircleShape> circles;

void handleValues() {
	i += !direction ? 1 : -1;

	if (i == LIMIT) {
		direction = true;
	} else if (i == 1) {
		direction = false;
	}
}

void drawCircles() {
	for (auto circle : circles) {
		window.draw(circle);
	}
}

void handleCircles() {
	if (!direction) {
		radius = Easing::easeOutCubic(i / LIMIT_F) * baseRadius;

		std::cout << radius << std::endl;

		sf::CircleShape circle;
		circle.setRadius(radius);
		circle.setPosition((WIDTH / 2) - radius, (HEIGHT / 2) - radius);
		circle.setOutlineColor(sf::Color::White);
		circle.setFillColor(sf::Color::Transparent);
		circle.setOutlineThickness(2.f);
		
		circles.push_back(circle);
	} else {
		circles.pop_back();
	}
}

int main() {
	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}

			if (event.type == sf::Event::TextEntered) {
				if (event.text.unicode == 113) {
					window.close();
				}
			}
		}

		window.clear();
		handleValues();
		handleCircles();
		drawCircles();
		window.display();

		sf::sleep(sf::milliseconds(FPS_60));
	}

	return 0;
}
