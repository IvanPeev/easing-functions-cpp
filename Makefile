CXXLIBS=-lsfml-graphics -lsfml-window -lsfml-system

all: main-obj easing-obj
	g++ -o main main.o easing.o $(CXXLIBS)

main-obj:
	g++ -c main.cpp $(CXXLIBS)

easing-obj:
	g++ -c easing.cpp $(CXXLIBS)

clean:
	rm -rf *.o main

.PHONY: all clean
