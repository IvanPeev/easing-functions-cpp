// https://gist.github.com/gre/1650294

namespace Easing {
	float linear(float);
	float easeInQuad(float);
	float easeOutQuad(float);
	float easeInOutQuad(float);
	float easeInCubic(float);
	float easeOutCubic(float);
	float easeInOutCubic(float);
	float easeInQuart(float);
	float easeOutQuart(float);
	float easeInOutQuart(float);
	float easeInQuint(float);
	float easeOutQuint(float);
	float easeInOutQuint(float);
}
